import React, {Component} from 'react';
import {
    Link
} from "react-router-dom";
import Modal from "./Modal";
import RequestAPI from "../../Helpers/RequestAPI";
import CheckoutBlock from "./CheckoutBlock";

class Checkout extends Component {

    DELIVERY_COST = 2.99;

    constructor(props) {
        super(props);
        this.state = {
            delivery_type: 'self_pickup',
            thankYouOpened: false,
        };

        this.changeDelivery             = this.changeDelivery.bind(this);
        this.handleSubmit               = this.handleSubmit.bind(this);
        this.handleFormChange           = this.handleFormChange.bind(this);
        this.closeOrderReceivedPopup    = this.closeOrderReceivedPopup.bind(this);
    }

    render() {
        if( this.props.cart.isEmpty() ) {
            return this.renderEmptyCart();
        }

        return(
            <div className={'checkout'}>

                <Modal open={this.state.thankYouOpened} onClose={this.closeOrderReceivedPopup}/>

                <form onSubmit={this.handleSubmit}>
                    {this.renderYourCartBlock()}
                    {this.renderPersonalInfoBlock()}
                    {this.renderDeliveryBlock()}
                    {this.renderPaymentBlock()}
                </form>

            </div>
        );
    }

    renderEmptyCart() {
        return (
            <div className={'checkout'}>
                <div className={'checkout__block'}>
                    <h3 className={'checkout__block-title _left'}>Your cart</h3>
                    <div className={'order'}>
                        {'Your cart is empty.'}
                        <div className={'order__btns'}>
                            <Link to={'/'}>
                                <button>Go to menu</button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderPizzaList() {
        let cart = this.props.cart;
        let cartItems = cart.getItems();

        let pizzaList = Object.keys(cartItems).map( ( id ) => {
            let item = cartItems[id];
            return (
                <div className={'position'} key={id}>
                    <div className={'position__image'}>
                        <img src={item.image_url}/>
                    </div>
                    <div className={'position__title'}>
                        {item.name}
                    </div>

                    {this.renderQuantityControls(item, id)}

                    <div className={'position__price'}>
                        {item.price_usd.toFixed(2)} $
                    </div>
                    <div className={'position__controls'}>
                        <button onClick={ ()=> cart.removeById(id) }>Remove</button>
                    </div>
                </div>
            );
        } );

        return pizzaList.length ? pizzaList : 'Your cart is empty.';
    }

    renderRemoveAllButton() {
        if( this.props.cart.isEmpty() ) {
            return false;
        } else {
            return(
                <div className={'_right'}>
                    <span className={'remove-all'} onClick={() => this.props.cart.removeAllItems()}>Remove all</span>
                </div>
            )
        }

    }

    renderQuantityControls(item,id) {
        let cart = this.props.cart;

        return (
            <div className={'position__quantity'}>
                <span onClick={ () => cart.decreaseQuantity(id) }
                      className={'quantity-control' + ( item.quantity <= 1 ?  ' disabled' : '' ) }
                >
                    -
                </span>
                <div className={'quantity__number'}>
                    {item.quantity}
                </div>
                <span onClick={ () => cart.increaseQuantity(id) }
                    className={'quantity-control'}
                >
                    +
                </span>
            </div>
        );
    }

    renderDeliveryBlock() {
        return (
            <CheckoutBlock title={'Delivery'}>
                <div className={'delivery-tabs _left'}>
                    <div className={'delivery-tabs__tab'
                    + ( this.state.delivery_type === 'self_pickup' ? '' : ' not-checked' ) }
                         onClick={ () => this.changeDelivery('self_pickup') }
                    >
                        Self Pickup (Free)
                    </div>

                    <div className={'delivery-tabs__tab'
                    + ( this.state.delivery_type === 'deliver' ? '' : ' not-checked' )}
                         onClick={ () => this.changeDelivery('deliver') }
                    >
                        Deliver to (${this.DELIVERY_COST})
                    </div>
                </div>
                {this.renderDeliveryBlockContent()}
            </CheckoutBlock>
        );
    }

    renderDeliveryBlockContent() {
        if( this.state.delivery_type === 'self_pickup') {
            return (
                    <div>
                        <p>You can pickup your order at this address: 7704 S. Green Lake St.
                            Ypsilanti, MI 48197</p>
                        <div className={'_right'}>
                            Delivery cost: Free
                        </div>
                    </div>
            );
        } else {
            return (
                <div>
                    <div className={'inputs-group'}>
                        <label htmlFor={'delivery_address_1'}>
                            Address line 1*
                            <input required type={'text'}
                                   name={'delivery_address_1'}
                                   id={'delivery_address_1'}
                                   onChange={this.handleFormChange}
                            />
                        </label>
                        <label htmlFor={'delivery_address_2'}>
                            Address line 2*
                            <input required type={'text'}
                                   name={'delivery_address_2'}
                                   id={'delivery_address_2'}
                                   onChange={this.handleFormChange}
                            />
                        </label>
                    </div>
                    <div className={'_right'}>
                        Delivery cost: $ {this.DELIVERY_COST}  /
                        € {this.props.cart.convertPriceTo('EUR', this.DELIVERY_COST).toFixed(2)}
                    </div>
                </div>
            );
        }
    }

    renderPersonalInfoBlock() {
        return(
            <CheckoutBlock title={'Personal Information'}>

                <div className={'inputs-group'}>
                    <label htmlFor={'client_name'}>
                        Your name*
                        <input required type={'text'}
                               name={'client_name'}
                               id={'client_name'}
                               onChange={this.handleFormChange}
                        />
                    </label>
                    <label htmlFor={'client_phone'}>
                        Phone*
                        <input required type={'tel'}
                               name={'client_phone'}
                               id={'client_phone'}
                               onChange={this.handleFormChange}
                        />
                    </label>
                    <label htmlFor={'client_email'}>
                        Email*
                        <input required
                               type={'email'}
                               name={'client_email'}
                               id={'client_email'}
                               onChange={this.handleFormChange}
                        />
                    </label>
                </div>
            </CheckoutBlock>
        );
    }

    renderYourCartBlock() {
        let cart = this.props.cart;
        return(
            <CheckoutBlock title={'Your cart'}>
                {this.renderRemoveAllButton()}
                <div className={'order'}>
                    {this.renderPizzaList()}
                </div>
                <div className={'_right'}>
                    Subtotal: $ {cart.getItemsTotalCost().toFixed(2)} /
                    € { cart.getItemsTotalCost('EUR').toFixed(2) }
                </div>
            </CheckoutBlock>
        );
    }

    renderPaymentBlock() {
        return (
            <CheckoutBlock title={'Payment'}>
                <div>Pay with cash or credit card on order receipt.</div>

                <div className={'order-buttons _right'}>
                    Total: $ {this.getTotal().toFixed(2)}  /
                    € {this.props.cart.convertPriceTo('EUR', this.getTotal() ).toFixed(2)}
                </div>

                <div className={'order-buttons _right'}>
                    <button type={'submit'}>Create order</button>
                </div>
            </CheckoutBlock>
        );
    }

    getTotal() {
        return this.props.cart.getItemsTotalCost() + ( this.state.delivery_type === 'deliver' ? 2.99 : 0);
    }

    handleFormChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    changeDelivery(delivery) {
        this.setState({delivery_type: delivery});
    }

    handleSubmit(event) {
        event.preventDefault();

        if( !this.validateOrder() ) {
            return;
        }

        let data = this.state;
        data.items_ordered = JSON.stringify(this.props.cart.getItems());
        data.status = 'create';

        RequestAPI('order/create', {
            method: 'POST',
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((data) => {
                this.setState({
                    thankYouOpened: true
                });
            })
            .catch((error) => {
                alert('Something went wrong.');
            });

    }

    validateOrder() {
        return true;
    }

    closeOrderReceivedPopup() {
        this.setState({
            thankYouOpened: false
        });
        this.props.cart.removeAllItems();
    }


}

export default Checkout;