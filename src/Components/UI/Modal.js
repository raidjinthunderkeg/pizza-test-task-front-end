import React from 'react';



function Modal(props) {
    if( props.open ) {
        return(
            <div className={'modal'}>
                <div className={'modal__inner'} >
                    <p>
                        Thank you, your order has been received and will be proccessed soon (or not, because this is a test app).
                    </p>
                    <button onClick={() => props.onClose()}>Close</button>
                </div>
            </div>

        );
    }

    return null;
}

export default Modal;