import React from 'react';
import {
    Link
} from "react-router-dom";


function Header(props) {
    return(
        <header>
            <div className={'header__container'}>
                <div className={'header__flex'}>

                    <a className={'header__logo'} href={'/'}>
                        <img src={'logo.png'}/>
                    </a>

                    <div className={'header__navigation'}>
                        <nav className={'navigation'}>
                            <Link to={'/'} className={'navigation__item'}>
                                Menu
                            </Link>
                            <Link to={'/cart'} className={'navigation__item'}>
                                Cart <span className={'cart-items-counter'}>{props.cartItemsCount}</span>
                            </Link>
                        </nav>
                    </div>

                </div>
            </div>
        </header>
    );
}

export default Header;