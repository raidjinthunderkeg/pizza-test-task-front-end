import React from 'react';
import CooldownButton from "./CooldownButton";

const getPriceLabel = function (price_usd) {
    return '$ ' + price_usd ;
};

function PizzaCard(props) {
    return(
        <li className={'pizza-card'}>
            <div className={'pizza-card__inner'}>
                <div className={'pizza-card__img'}>
                    <img src={props.pizza.image_url}/>
                </div>
                <div className={'pizza-card__name'}>
                    {props.pizza.name}
                </div>
                <div className={'pizza-card__desc'}>
                    {props.pizza.description}
                </div>
                <div className={'pizza-card__price'}>
                    { getPriceLabel( props.pizza.price_usd.toFixed(2) ) }
                </div>
                <div className={'pizza-card__add-to-cart'}>
                    <CooldownButton onClick={() => props.onAdd(props.pizza) }
                                    cooldownText={'Added'}
                    >
                        Add to cart
                    </CooldownButton>
                </div>
            </div>
        </li>
    );
}

export default PizzaCard;