import React from 'react';

function Footer() {
    return(
        <footer>
            <div className={'footer__block'}>
                <div className={'footer_item'}>
                    Made by <a href={'https://t.me/selean_d'}>Selean</a> for Innoscripta
                </div>
                <div className={'footer_item'}>
                    Email: <a href={'mailto:d.selean@gmail.com'}>d.selean@gmail.com</a>
                </div>
            </div>
        </footer>
    );
}

export default Footer;