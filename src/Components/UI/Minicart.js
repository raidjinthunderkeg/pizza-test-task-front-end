import React from 'react';
import cartLogo from './../../shopping-cart.svg';
import {Link} from "react-router-dom";

function Minicart(props) {
    if( props.cart.isEmpty() ) {
        return null;
    }
    let priceUSD = props.cart.getItemsTotalCost().toFixed(2);
    return(
        <Link to={'/cart'} className={'minicart'}>

            <div className={'minicart__inner'}>
                <img src={cartLogo}  alt="logo" />
                <div className={'minicart__totals'}>$ {priceUSD}</div>
            </div>

        </Link>
    );
}

export default Minicart;