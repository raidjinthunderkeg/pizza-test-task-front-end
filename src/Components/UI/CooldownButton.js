import React, {Component} from 'react';

class CooldownButton extends Component {

    timer;

    constructor(props) {
        super(props);
        this.state = {
            onCooldown: false
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        if( this.state.onCooldown ) {
            return;
        }

        this.props.onClick();
        this.setState({ onCooldown: true} );
        this.timer = setTimeout(() => {
           this.setState({ onCooldown: false});
        }, 1500);
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
    }

    render() {
        return(
            <button style={this.state.onCooldown ? {pointerEvents: 'none'} : {} }
                    onClick={this.handleClick}
            >
                { this.state.onCooldown ? this.props.cooldownText : this.props.children}
            </button>
        );
    }



}

export default CooldownButton;