import React, {Component} from 'react';
import PizzaCard from "./PizzaCard";
import pizzaJSON from './../../json/pizza_json_example';
import RequestAPI from "../../Helpers/RequestAPI";

class Menu extends Component {

    componentDidMount() {

        let pizzaData;

        RequestAPI('pizza/get')
            .then(results => {
                return results.json();
            })
            .then(data => {
                pizzaData = data;
            }).catch(reason => {
            console.log(reason);
        });

        if( ! pizzaData ) {
            pizzaData = pizzaJSON.data;
        }

        this.setState({
            pizzas: pizzaData
        } );
    }

    render() {
        return(
            <div className={'menu'}>
                <h2 className={'_left'}>Menu</h2>
                <ul>
                    {this.renderPizzaList()}
                </ul>
            </div>
        );
    }

    renderPizzaList() {
        let pizzaList = 'Loading';
        if( this.state ) {
            pizzaList = this.state.pizzas.map( (pizza, index) =>
                <PizzaCard key={index} pizza={pizza} onAdd={this.props.onAdd}/>
            );
        }

        return pizzaList;
    }


}

export default Menu;