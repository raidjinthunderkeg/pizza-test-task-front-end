import React from 'react';

function CheckoutBlock(props) {
    return(
        <div className={'checkout__block'}>
            <h3 className={'checkout__block-title _left'}>{props.title}</h3>
            {props.children}
        </div>
    );
}

export default CheckoutBlock;