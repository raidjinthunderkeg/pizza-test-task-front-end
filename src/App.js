import React, {Component} from 'react';
import './App.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import Header from "./Components/UI/Header";
import Footer from "./Components/UI/Footer";
import Menu from "./Components/UI/Menu";
import Cart from "./Cart";
import Minicart from "./Components/UI/Minicart";
import Checkout from "./Components/UI/Checkout";
import ScrollToTop from "./Components/ScrollToTop";

class App extends Component {

    constructor(props) {
        super(props);

        this.forceUpdate = this.forceUpdate.bind(this);
        this.state = {
            cart: new Cart({
                onChange: this.forceUpdate
            })
        };
    }

    render() {
        return (
            <Router>
                <ScrollToTop />
                <div className="App">
                    <Header cartItemsCount={this.state.cart.getItemsCount()}/>
                    <div className={'content'}>

                        <Switch>
                            <Route exact path="/">
                                <Menu onAdd={ (pizza) => this.state.cart.add(pizza) } />
                                <Minicart cart={this.state.cart} />
                            </Route>
                            <Route path="/cart">
                                <Checkout cart={this.state.cart} />
                            </Route>
                        </Switch>

                    </div>
                    <Footer/>
                </div>
            </Router>
        );
    }

}

export default App;
