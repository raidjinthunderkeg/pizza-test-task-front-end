
function RequestAPI( endpoint, args = { method: 'GET' } ) {
    let url = window.location.origin + '/api/' + endpoint;
    if( args.method === 'POST' ) {
        return fetch(url, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(args.body)
        })
    } else {
        return fetch( url )
    }
}

export default RequestAPI;