
class Cart {

    exchangeRates;

    _items;
    _LSTORAGE_KEY = 'cart';
    _onChange;


    constructor(options) {
        this._onChange = options.onChange || null;
        this._items = JSON.parse(localStorage.getItem(this._LSTORAGE_KEY)) || {};
        this.fetchExchangeRates();
    }

    fetchExchangeRates() {
        fetch('https://api.exchangeratesapi.io/latest?base=USD&symbols=EUR')
            .then((response)=>response.json() )
            .then((data) => {
                this.exchangeRates = {
                    USD: {
                        to: {
                            EUR: data.rates.EUR
                        }
                    }
                };
                this._change();
            });

    }

    add( pizza ) {
        if( this._items[pizza.id] !== undefined ) {
            this._items[pizza.id].quantity += 1;
        } else {
            this._items[pizza.id] = {
                quantity: 1,
                ...pizza
            }
        }
        this._change();

        return this;
    }

    removeById(id) {
        delete this._items[id];
        this._change();
        return this;
    }

    increaseQuantity( id ) {
        if( this.getItem( id ) ) {
            this._items[id].quantity += 1;
            this._change();
        }

        return this;
    }

    decreaseQuantity( id ) {
        if( this.getItem( id ) ) {
            this._items[id].quantity -= 1;
            if( this._items[id].quantity === 0 ) {
                delete this._items[id];
            }

            this._change();
        }

        return this;
    }

    getItem( id ) {
        return  this._items[id] || null;
    }

    _change() {
        this._onChange();
        this._save();

        return this;
    }

    _save() {
        localStorage.setItem( this._LSTORAGE_KEY, JSON.stringify(this._items) );

        return this;
    }

    getItems() {
        return this._items;
    }

    getItemsCount() {
        let count = 0;
        Object.values(this._items).forEach( item => {
            count += item.quantity;
        });
        return count;
    }

    getItemsTotalCost(currency = 'USD') {
        let cost = 0;
        Object.values(this._items).forEach( item => {
            cost += item.price_usd * item.quantity;
        });

        return this.convertPriceTo(currency, cost);
    }

    getItemsTotalCostRounded(currency = 'USD') {
        return Math.round(this.getItemsTotalCost(currency));
    }

    removeAllItems() {
        this._items = {};
        this._change();

        return this;
    }

    isEmpty() {
        return Object.entries(this._items).length <= 0;
    }

    convertPriceTo(currency, price) {
        if( this.exchangeRates && this.exchangeRates.USD.to[currency] ) {
            return price * this.exchangeRates.USD.to[currency];
        }

        return price;
    }

}

export default Cart;